#include <stdio.h>
#include <stdlib.h>
#include <libwnck/libwnck.h>
#include <gdk/gdk.h>
#include <getopt.h>

/**
 * Is used to hold all information about a Window.
 */
typedef struct Window {
  int x;
  int y;
  int width;
  int height;
  WnckWindow *window;
  int workspaceNumber;
  int pid;
  int xid;  
  const char *name;
  WnckWindowState windowState;
  WnckWindowType windowType;
} Window;

/**
 * Is used to hold all information about a Monitor.
 */
typedef struct Monitor {
  int xOffset;
  int yOffset;
  int width;
  int height;
} Monitor;

/**
 * Is used to move and resize a Window.
 *
 * @param window is the window which will be moved and resized.
 * @param x is the new x coordinate.
 * @param y is the new y coordinate.
 * @param width is the new width.
 * @param height is the new height.
 */
void move_resize(WnckWindow *window, int x, int y, int width, int height) {
  wnck_window_unmaximize_horizontally(window);
  wnck_window_unmaximize_vertically(window);
  wnck_window_set_geometry(window,
			   WNCK_WINDOW_GRAVITY_STATIC,
			   WNCK_WINDOW_CHANGE_X 
			   + WNCK_WINDOW_CHANGE_Y 
			   + WNCK_WINDOW_CHANGE_WIDTH 
			   + WNCK_WINDOW_CHANGE_HEIGHT,
			   x,
			   y,
			   width,
			   height);
}

/**
 * Takes a WnckWindow and creates a Window out of it.
 *
 * @param wnckWindow is a `WnckWindow`.
 *
 * @return a `Window` based on the wnckWindow.
 */
Window *createWindow(WnckWindow *wnckWindow){
  const char *name = wnck_window_get_class_group_name (wnckWindow);
  WnckWorkspace *workspace = wnck_window_get_workspace (wnckWindow);
  
  int x, y, width, height;
  wnck_window_get_geometry(wnckWindow, &x, &y, &width, &height);
  WnckWindowType wnckWindowType = wnck_window_get_window_type (wnckWindow);
  int pid = wnck_window_get_pid(wnckWindow);
  int xid = wnck_window_get_xid(wnckWindow);  
  Window *window;
  window = (Window*)malloc(sizeof(Window));
  window->name = name;
  window->pid = pid;
  window->xid = xid;    
  window->x = x;
  window->y = y;
  window->width = width;
  window->height = height;
  window->window = wnckWindow;
  window->windowType = wnckWindowType;
  if (workspace == NULL)
    window->workspaceNumber = -1;
  else
    window->workspaceNumber = wnck_workspace_get_number (workspace);

  return window;
}

/**
 * Checks wheter a `Window` is located on a `Monitor`.
 *
 * @param window is the window for which will be checked
 *               if it is located on monitor.
 * @param monitor is the Monitor for which will be
 *                checked if the window is located on it.
 */
gboolean onMonitor(Window *window, Monitor *monitor){
  return monitor->xOffset <= window->x
    && window->x < (monitor->xOffset + monitor->width)
    && monitor->yOffset <= window->y
    && window->y < (monitor->yOffset + monitor->height);  
}

/**
 * Creates a Monitor for a given `Window`.
 *
 * @param window is `Window` for which a `Monitor` is created.
 *
 * @return monitor is `Monitor` on which window is located.
 */
Monitor *getMonitor(Window *window){
  GdkDisplay *defaultDisplay = gdk_display_get_default ();
  GdkMonitor *defaultMonitor =
    gdk_display_get_monitor_at_point(defaultDisplay, window->x, window->y);
  GdkRectangle geometry = { .x = 0, .y = 0, .width = 0, .height = 0 };
  gdk_monitor_get_geometry (defaultMonitor, &geometry);

  Monitor *monitor;
  monitor = (Monitor*)malloc(sizeof(Monitor));
  monitor->xOffset = geometry.x;
  monitor->yOffset = geometry.y;
  monitor->width = geometry.width;
  monitor->height = geometry.height;
  return monitor;
}

/**
 * Takes a `Window` and a `Monitor` and a `GList` and modifies
 * the Monitors usable area to skip panel areas.
 *
 * @param panel is the `Window` which should be avoided in the
 *              usable area of the Monitor.
 * @param monitor is the `Monitor` which usable area will be
 *                updated to avoid the panel.
 * @param ignore is `GList` of `Window` which will be compared
 *               to panel. If panel is in ignore monitor will
 *               not be updated.
 */
void adjustMonitor(Window *panel, Monitor *monitor, GList *panelsToIgnore){
  for (; panelsToIgnore!=NULL; panelsToIgnore=panelsToIgnore->next){
    if (0==strcmp(panelsToIgnore->data, panel->name))
      printf("Ignore Panel: %s\n", panel->name);
    else {
      if (panel->width == monitor->width){
	//top or bottom
	if (panel->y == monitor->yOffset)
	  monitor->yOffset += panel->height;
	else if (panel->y == monitor->yOffset + monitor->height - panel->height)
	  monitor->height -= panel->height;
	else {
	  printf("ERROR: %s\n", "Couldn't calculate horizontal Panel.");
	}
      }else if (panel->height == monitor->height){
	//left or right
	if (panel->x == monitor->xOffset)
	  monitor->xOffset += panel->width;
	else if (panel->x == monitor->xOffset + monitor->width - panel->width)
	  monitor->width -= panel->width;
	else
	  printf("ERROR: %s\n", "Couldn't calculate vertical Panel.");
      } else {
	//do nothing
	printf("%s\n", "No panel.");
      }
    }
  }
}

/**
 * checks if two `Window` are located on the same Workspace.
 *
 * @param main is the main `Window` against another Window
 *             is compared to.
 * @param window is the window which is compared against main.
 *
 * @return gboolean if main and window are on the same monitor
 *                  are.
 */
gboolean onWorkspace(Window *main, Window *window){
  if (window->workspaceNumber == -1)
    return TRUE;
  return main->workspaceNumber == window->workspaceNumber;
}

/**
 * Creates and returns a `GList` pointer of `Window` which are all the passive
 * Windows for a given Monitor and a main window by potentially ignoring a
 * number of Windows.
 *
 * @param monitor specifies the monitor on which all returned windows are
 *                located.
 * @param ignore specifies all WnckWindows to ignore.
 * @param wnckScreen is used to aquire all WnckWindows for current
 *                   WnckWorkspace.
 * @param main is the main window with which all the other windows share
 *                     the same Monitor.
 * @return GList* returns GList of Window.
 */
GList *getWindows(Monitor *monitor, GList *ignore, WnckScreen *wnckScreen, Window *main){
  GList *wnckWindows =  wnck_screen_get_windows (wnckScreen);
  gboolean isOnWorkspace;
  gboolean isActive;
  gboolean isOnMonitor;
  gboolean isDock;
  gboolean isMinimized;
    
  GList *windows = NULL;
  for (; wnckWindows != NULL; wnckWindows = wnckWindows->next){
    WnckWindow *wnckWindow = wnckWindows->data;  
    Window *window = createWindow(wnckWindow);

    isActive = window->window == main->window;
    isDock = window->windowType == WNCK_WINDOW_DOCK;
    isOnWorkspace = onWorkspace(main, window);
    isOnMonitor = onMonitor(window, monitor);
    isMinimized = wnck_window_is_minimized (wnckWindow);
    
    if (isOnWorkspace && !(isActive) && isOnMonitor && !(isDock) && !(isMinimized))
      windows = g_list_append(windows, window);
    else if (isDock && isOnWorkspace && isOnMonitor)
      adjustMonitor(window, monitor, ignore);
  }
  return windows;
}

/**
 * Takes a GList of `Window` and a separate main `Window` and places
 * theme in a focus to main either left or right and tiled for the
 * rest on the other monitor half fashion.
 *
 * @param main is the main Window which is placed half monitor either
 *             left or right.
 * @param windows is the GList of `Window` which are aranged in the
 *                unfocussed half of the monitor.
 * @param monitor is the `Monitor` on which the Windows get arranged.
 * @param left specifies wether the main focus is the left or right
 *             half of the monitor.
 */
void tile(Window *main, GList *windows, Monitor *monitor, gboolean left){
  if (windows == NULL)
    wnck_window_maximize (main->window);
  else if (1 == g_list_length (windows)){
    int height = monitor->height - monitor->yOffset;
    if (left){
      move_resize(main->window, monitor->xOffset, monitor->yOffset,
	   monitor->width/2, height);
      Window *window = g_list_first(windows)->data;
      move_resize(window->window, monitor->xOffset + monitor->width/2, monitor->yOffset,
	   monitor->width/2, height);
    } else {
      move_resize(main->window, monitor->xOffset + monitor->width/2,
	   monitor->yOffset, monitor->width/2, height);
      Window *window = g_list_first(windows)->data;
      move_resize(window->window, monitor->xOffset, monitor->yOffset,
	   monitor->width/2, height);
    }
  } else {
    int count = g_list_length(windows);
    int height = (monitor->height - monitor->yOffset)/count;
    int heightLast;
    if (height * count < (monitor->height - monitor->yOffset))
      heightLast = height += (monitor->height - monitor->yOffset) - height * count;
    else
      heightLast = height;
    if (left){
      move_resize(main->window, monitor->xOffset, monitor->yOffset,
	   monitor->width/2, monitor->height);
      int counter = 1;
      for (; windows!=NULL; windows=windows->next){
	Window *window = windows->data;
	if (count == counter)
	  height = heightLast;
	move_resize(window->window, monitor->xOffset + monitor->width/2,
	     monitor->yOffset, monitor->width/2, height);
	monitor->yOffset += height;
	counter++;
	}
    } else {
      move_resize(main->window, monitor->xOffset + monitor->width/2,
	   monitor->yOffset, monitor->width/2, monitor->height);
      int counter = 1;
      for (; windows!=NULL; windows=windows->next){
	Window *window = windows->data;
	if (count == counter)
	  height = heightLast;
	move_resize(window->window, monitor->xOffset,
	     monitor->yOffset, monitor->width/2, height);
	monitor->yOffset += height;
	counter++;
      }
    }
  }
}

/**
 * prints the usage of the application.
 */
void print_usage(){
  printf("Usage: tiling-app -l | tiling-app -r \n");
}

int main(int argc, char **argv){
  gdk_init(&argc, &argv);

  WnckScreen *wnckScreen = wnck_screen_get_default();
  wnck_screen_force_update(wnckScreen);
  WnckWindow *activeWnckWindow = wnck_screen_get_active_window(wnckScreen);
  Window *activeWindow = createWindow(activeWnckWindow);
  Monitor *monitor = getMonitor(activeWindow);

  GList *panelsToIgnore = NULL;
  panelsToIgnore = g_list_append(panelsToIgnore, "Budgie-panel");

  GList *windows = getWindows(monitor, panelsToIgnore, wnckScreen, activeWindow);

  int option;
  if (argc == 1){
    print_usage();
    return 0;
  } else {
    while ((option = getopt(argc, argv, "lr")) != -1){
      switch (option) {
      case 'l':
	tile(activeWindow, windows, monitor, TRUE);
	break;
      case 'r':
	tile(activeWindow, windows, monitor, FALSE);
	break;
      default:
	print_usage();
      }
    }
    return 0;
  }
}
