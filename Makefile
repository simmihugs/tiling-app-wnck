CC=/usr/bin/gcc
PKG_CONFIG=`pkg-config --cflags libwnck-3.0`
LIBS=`pkg-config --libs libwnck-3.0`
CFLAGS=-Wno-unused-variable -Wall -O2 -DWNCK_I_KNOW_THIS_IS_UNSTABLE $(PKG_CONFIG)

all: tiling-app

tiling-app: 
	$(CC) $(CFLAGS) $(LIBS) -o build/tiling-app src/main.c 

